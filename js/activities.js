// Om vi väljer en eventbild skall vi hantera detta nu
// URL: http://www.tinymce.com/tryit/imagemanager/standalone_integration.php	
function eventsCustomInsert(data) 
{
	
	var filename	=	data.focusedFile.url;
	var filesize	=	data.focusedFile.size;
	var str			=	'';
	
	// Gör sökvägen (filename) relativ -> "uploads/images/...."
	str	=	'uploads' +filename.split('uploads')[1];
	
	// Skriver in sökvägen till bilden i inputfältet
	$('#event_img').val(str);
	
	// Visar bilden som vi precis har valt
	$('#preview_img').attr('src', filename).show(500);
		
}
// Om vi vill ta bort bilden skall vi hantera detta nu
// URL: http://www.tinymce.com/tryit/imagemanager/standalone_integration.php	
function eventsDeleteFile() 
{
	
	// Skriver in sökvägen till bilden i inputfältet
	$('#event_img').val('');
	
	// Tar bort bilden som vi precis har valt
	$('#preview_img').attr('src', '').hide(500);
}