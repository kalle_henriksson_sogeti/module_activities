<?PHP
	error_reporting(E_ALL);
	ini_set('display_errors', '1');

	if (!defined('BASEPATH')) exit('No direct script access allowed');


	
	
/**
@Module:		Activities
@Name:			admin.php
--------------------------------------------------------------------------------------------------
@Creator:		Sogeti, Kalle Henriksson
@Created:		2015
@Version:		1.0
@PHP Version: 	5	
--------------------------------------------------------------------------------------------------
@Description	Denna fil hanterar aktiviteter som kan ingå vid ett event.

@History
DATE			AUTHOR				ACTION
2015-03-09		Kalle Henriksson	Skapade modulen.
	
*/
class Admin extends Admin_Controller 
{				  
	
	function __construct()
	{
		
		parent::Admin_Controller();
		
		// Hämtar in nödvändiga filer
		
		// Läser in libraries
		$this->load->library(array(
			'form_validation',
		));
			
		// Läser in models
		$this->load->model(array(
			'language_model',
			'events/events_model',
			'activities/activities_model',
			'contact/contact_model',
			'accounts/accounts_model',
		));
		
		// Läser in helpers
		$this->load->helper(array(
			'language',
			'url',
			'events/events',
			'activities',
			'contact/contact',
			'form',
			'accounts/accounts',
		));
		
	}
	
	/**
	@Name:			function index()
	------------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			none
	
	@Return								
	------------------------------------------------------------------------------------------------------------------
	@Description: 	Hämtar in alla aktiviteter och skriver ut dessa i en lista.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade funktionen.
		
	*/
	function index()
	{
		
		// Hämtar in registrerade moduler.
		$data['activities']	=	$this->activities_model->get_all_activities();
		
		// Skriver ut vyn där modulerna listas upp i en tabell.
		$this->template->build('activitieslist', $data);	
	}







	
	/**
	@Name:			add_activity()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Visar ett formulär för att registrera en ny aktivitet i systemet.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen
		
	*/
	function add_activity()
	{
		$data['accounts']		= $this->accounts_model->get_accounts();

		// Skriver ut vyn med ett formulär för att registrera nya aktiviteter i systemet			
		$this->template->build('edit_activity', $data);
		
	}






	/**
	@Name:			edit_activity()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Visar ett formulär för att redigera en aktivitet.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen
		
	*/
	function edit_activity(){
		// Hämtar ID för aktuell aktivitet
		$ea_id					= $this->uri->segment(4);
		
		$data['accounts']		= $this->accounts_model->get_accounts();

		// Hämtar aktivitet från DB
		$data['activity'] 		= array($this->activities_model->get_activity($ea_id));


		// Skriver ut vyn med ett formulär för att redigera en aktivitet			
		$this->template->build('edit_activity', $data);
	}











	/**
	@Name:			save_activity()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return					
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Hanterar både uppdatering av befintligt samt skapande av ny aktivitet.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen
		
	*/
	function save_activity(){

		// Läser in aktiva språk för att kunna registrera/uppdatera posten på samtliga aktiva språk
		$langs = $this->language_model->get_available_languages();
		$ea_id = $this->input->post('ea_id');

		// TABELLEN EVENTS
		// Sätter variabler för posten
		$data = array(
			'ea_account_id' 				=> $this->input->post('ea_account_id'),
			'ea_published' 					=> $this->input->post('ea_published'),
			'ea_updated' 					=> date("Y-m-d H:i:s"),
			'ea_updated_by' 				=> $this->session->userdata('user_id'),
		);

		if($ea_id != ""){
			// Uppdaterar posten		

			$this->db->where('ea_id', $ea_id)->update('events_activities', $data);
		}else{
			$data["ea_created"] = date("Y-m-d H:i:s");
			$data["ea_created_by"] = $this->session->userdata('user_id');
			$data["ea_status"] = 1;
			// Sparar ny post		

		
			$this->db->insert('events_activities', $data);
			
			// Sätter id för den post vi just skapade
			$ea_id = $this->db->insert_id();
		}


		
		// Uppdaterar postens innehåll (i tabellen events_activities_content) för varje språk...
		foreach($langs as $lang)
		{
			$eac_id						=	$this->input->post('eac_id');
			$eac_title					=	$this->input->post('eac_title');
			$eac_description			=	$this->input->post('eac_description');
			$eac_seo_keywords			=	$this->input->post('eac_seo_keywords');
			$eac_seo_description		=	$this->input->post('eac_seo_description');
			$eac_language				=	$lang->language_code;
						

			$ec = array(
			   'eac_ea_id' 				=> $ea_id,
			   'eac_title' 				=> $eac_title,
			   'eac_description' 		=> $eac_description,
			   'eac_seo_keywords' 		=> $eac_seo_keywords,
			   'eac_seo_description' 	=> $eac_seo_description,
			   'eac_language'			=> $eac_language
			);				

			// Har vi ett eac_id skall vi UPPDATERA posten
			if($eac_id != "") {								
				$this->db->where('eac_id', $eac_id);				
				$this->db->update('events_activities_content', $ec); 
			} else {
				$this->db->insert('events_activities_content', $ec);
			}
		}


				
		// Feedback
		$this->session->set_flashdata('msg', '<div class="message success"><p>Aktiviteten är sparat.</p></div>');
		
		// Visar vyn igen
		redirect('admin/activities', 'refresh');
	}



	


	/**
	@Name:			function events_action()
	------------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Return			object					
	------------------------------------------------------------------------------------------------------------------
	@Description: 	Administrerar ett event. Används uteslutande av systemadministratören.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen.
		
	*/
	
//	function events_action() {
//
//		$do 	= $this->input->post('do');
//		$item 	= $this->input->post('item');
//		
//		// Skall vi ta bort poster. Observera att vi inte raderar posterna ännu.
//		// Vi sätter _status till 0 för att kunna ångra oss.
//		if(isset($do['delete']))
//		{			
//			foreach($item as $id)
//			{
//				$this->db->where('event_id', $id)->update('events', array('event_status' => '0'));
//			}
//			
//			// Feedback.
//			$this->session->set_flashdata('msg', '<div class="message success">Markerade events inaktiverades.</div>');
//		
//		} elseif(isset($do['change_published'])) {
//
//			// Skall vi byta status (läst/oläst) på posterna gör vi det nu.
//			foreach($item as $id)
//			{
//				$this->db->where('event_id', $id)->update('events', array('event_published' => $this->input->post('event_published')));
//			}
//			
//			// Feedback.
//			$this->session->set_flashdata('message', '<div class="message success">Publiceringsstatus bytt</div>');		
//		}
//		
//		// Läser in vyn igen.
//		redirect($_SERVER['HTTP_REFERER'], 'refresh');
//	}		
	


	/**
	@Name:			function activities_action()
	------------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Return								
	------------------------------------------------------------------------------------------------------------------
	@Description: 	Administrerar liståtgärder för aktiviteter. Används uteslutande av systemadministratören.
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-24		Kalle Henriksson	Skapade funktionen.
		
	*/
	
	function activities_action() {

		$do 	= $this->input->post('do');
		$item 	= $this->input->post('item');
		
		// Skall vi ta bort poster. Observera att vi inte raderar posterna ännu.
		// Vi sätter _status till 0 för att kunna ångra oss.
		if(isset($do['delete']))
		{			
			foreach($item as $id)
			{
				$this->db->where('ea_id', $id)->update('events_activities', array('ea_status' => '0'));
			}
			
			// Feedback.
			$this->session->set_flashdata('msg', '<div class="message success">Markerade aktiviteter inaktiverades.</div>');
		
		} elseif(isset($do['change_published'])) {

			// Skall vi byta status (läst/oläst) på posterna gör vi det nu.
			foreach($item as $id)
			{
				$this->db->where('ea_id', $id)->update('events_activities', array('ea_published' => $this->input->post('ea_published')));
			}
			
			// Feedback.
			$this->session->set_flashdata('message', '<div class="message success">Publiceringsstatus bytt</div>');		
		}
		
		// Läser in vyn igen.
		redirect($_SERVER['HTTP_REFERER'], 'refresh');
	}		
}

?>