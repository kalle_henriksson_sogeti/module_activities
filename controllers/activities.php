<?PHP

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	

	/**
	@Module:		Manager
	@Name:			manager.php
	--------------------------------------------------------------------------------------------------
	@Creator:		Luckylane, Johan Lindqvist
	@Created:		2014
	@Version:		1.0
	@PHP Version: 	5	
	--------------------------------------------------------------------------------------------------
	@Description	Denna fil hanterar kontoadministratörens tjänster. Skalskyddet för modulen hanteras av
					luckycms/modules/core/users och anropas via $this->auth->user_logged_in().
	
	@History
	DATUM			VEM						ÅTGÄRD
	2014-05-05		Johan Lindqvist			Skapade filen	
	
	*/
	
class Events extends Public_Controller
{
	
	function __construct()
	{
		
		parent::Public_Controller();
				
		// Laddar in nödvändiga bibliotek och filer.
		
		// Läser in bibliotek för skalskydd.
		$this->load->library(array('auth','form_validation','email'));

		// Läser in models.
//		$this->load->model(array('events/events_model', 'contact/contact_model', 'accounts/accounts_model','sms/sms_model'));
		
		// Läser in helpers.
//		$this->load->helper(array('events/events', 'contact/contact','accounts/accounts','sms/sms'));

		// Läser in models
		$this->load->model(array('language_model','events/events_model','contact/contact_model','accounts/accounts_model'));
		
		// Läser in helpers
		$this->load->helper(array('language','url','events','contact/contact','form','accounts/accounts'));
		
		// Läser in språk.
		$this->lang->load('events', $this->session->userdata('language_name'));
		$this->lang->load('accounts/accounts', $this->session->userdata('language_name'));
		$this->lang->load('sms/sms', $this->session->userdata('language_name'));
		$this->lang->load('contact/contact', $this->session->userdata('language_name'));
		
	}
	
	// Är användaren inloggad visas vyn dashboard annars visas vyn för inloggning.
	function index()
	{
		// Hämtar in publicerade och aktiva poster för aktuellt språk
		$data['events_list']		=	$this->events_model->get_events();
				
		// Sätter variabler för att kunna presentera layouten
		$data['cur_slug']		=	$this->uri->segment(1);		
		$data['default_index'] 	= 	$this->site_options['index_page'];
        $this->template->set_theme($this->site_options['site_theme']);
		
        $this->template->set_layout('layout');
				 
		// Läser in vyn som skall presentera posterna                                
        $this->template->title(lang('events_headline'))->build('public_eventslist', $data);
	}

	function events(){

	}

	function event(){

	}

	function get_image(){
		$event_id	=	$this->uri->segment(3);
		$new_width	=	$this->uri->segment(4);
		$new_height	=	$this->uri->segment(5);

		$eventImage = $this->events_model->get_event_image_resized($event_id, $new_width, $new_height);

        if($eventImage == null){
        	die("An error ocurred when aquiring image.");
        }

        $mime_type = $eventImage->getMimeType();

        //	Baserat på mime-typ, returnera rätt typ av bild.
	    header('Content-Type: ' . $mime_type);
	    switch ($mime_type) {
	    	case 'image/jpeg':
	    imagejpeg($eventImage->getImage());
	    		break;
	    	case 'image/gif':
	    imagegif($eventImage->getImage());
	    		break;
	    	case 'image/png':
	    imagepng($eventImage->getImage());
	    		break;
	    	
	    	default:
	    		break;
	    }

	    imagedestroy($eventImage);
	}


	//	En ingång för att få ut eventlistan i json-format. Tänkt för appens eventlista.
	function events_json(){
		// Hämtar in publicerade och aktiva poster för aktuellt språk
		$event_datas = $this->events_model->get_events();
		$events_list = array();

		// Bygger en bantad lista med enbart det som appen är intrsserad av.
		foreach ($event_datas as $e) {
			$events_list[] = array(
				"id" => $e->event_id,
				"start_date" => strtotime($e->event_start_date . "T00:00:00+01:00"),
				"end_date" => strtotime($e->event_end_date . "T23:59:59+01:00"),
				"city" => "[STAD]",
				"image_data" => $this->events_model->get_event_image_resized($e->event_id, 75, 75)->getBase64(),
				"title" => $e->ec_title,
//				"description" => $e->ec_description,
			);
		}

		echo json_encode($events_list);
	}
	
}

?>