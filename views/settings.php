<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	 
	/**
	@Module:		Modules
	@Name:			settings.php
	---------------------------------------------------------------------------------------------------------------
	@Creator:		Luckylane, Johan Lindqvist
	@Created:		2014
	@Version:		1.0
	@PHP Version: 	5	
	---------------------------------------------------------------------------------------------------------------
	@Description	Denna fil visar modulens inställningar.
	
	@History
	DATE			AUTHOR				ACTION
	2014-10-14		Johan Lindqvist		Skapade filen.
		
	*/

?>
<h1><?PHP echo lang('events_headline'); ?> » <?PHP echo lang('system_settings'); ?></h1>
<?PHP 

	// Feedback
	echo $this->session->flashdata('message'); 
	
?>