<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed'); 
	
	/**
	@Module:		Activities
	@Name:			add_activity.php
	--------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	--------------------------------------------------------------------------------------------------
	@Description	Hanterar registrering av en aktivitet.
					
	@History
	DATUM			VEM						ÅTGÄRD
	2015-03-09		Kalle Henriksson		Skapade filen	
	
	*/


?>
<h1><?PHP echo lang('activities_headline'); ?></h1>
<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	// Feedback
	echo $this->session->flashdata('message');
	
	echo validation_errors(); 

	$has_ea = isset($activity) && count($activity) == 1;

	$ea = $has_ea ? $activity[0] : null;

	$ea_id							=	($has_ea ? $ea->ea_id : "");
	$ea_account_id					=	($has_ea ? $ea->ea_account_id : "");
	$ea_published					=	($has_ea ? $ea->ea_published : "");
	$ea_created						=	($has_ea ? $ea->ea_created : "");
	$ea_created_by					=	($has_ea ? $ea->ea_created_by : "");
	$ea_updated						=	($has_ea ? $ea->ea_updated : "");
	$ea_updated_by					=	($has_ea ? $ea->ea_updated_by : "");

	$eac_id							=	($has_ea ? $ea->eac_id : "");
	$eac_title						=	($has_ea ? $ea->eac_title : "");
	$eac_description				=	($has_ea ? $ea->eac_description : "");
	$eac_seo_keywords				=	($has_ea ? $ea->eac_seo_keywords : "");
	$eac_seo_description			=	($has_ea ? $ea->eac_seo_description : "");
	
?>
<div class="content_box">

	<div class="box_header">
		<?php if ($has_ea) { ?>
			<h4><?PHP echo lang('activities_edit_activity'); ?></h4>
		<?php } else { ?>
	    	<h4><?PHP echo lang('activities_add_activity'); ?></h4>
		<?php } ?>
    </div>
    
    <div class="box_content">
    	<?php 

			echo form_open('admin/activities/save_activity', array('class'=>'form label-top'));

		?>
			<input type="hidden" name="ea_id" value="<?php echo $ea_id; ?>" />
			<input type="hidden" name="eac_id" value="<?php echo $eac_id; ?>">

            <!-- TITEL -->
    		<div class="field">
    			<label class="required" for="eac_title"><?PHP echo lang('system_title'); ?></label>
    			<input type="text" class="xlarge" maxlength="50" name="eac_title" id="eac_title" value="<?php if($this->session->flashdata('eac_title')) { echo $this->session->flashdata('eac_title');} else { echo $eac_title; } ?>" />
    		</div>
            
            <!-- BESKRIVNING -->
    		<div class="field">
    			<label class="required" for="eac_description"><?PHP echo lang('system_description'); ?></label>
    			<textarea  class="wysiwyg xlarge" rel="tiny" cols="50" rows="7" name="eac_description" id="eac_description"><?php if($this->session->flashdata('eac_description')) { echo $this->session->flashdata('eac_description');} else { echo $eac_description; } ?></textarea>
    		</div>

            <!-- SEO-NYCKELORD -->
    		<div class="field">
    			<label class="" for="eac_seo_keywords"><?PHP echo lang('activities_seo_keywords'); ?></label>
    			<input type="text" class="xlarge" maxlength="50" name="eac_seo_keywords" id="eac_seo_keywords" value="<?php if($this->session->flashdata('eac_seo_keywords')) { echo $this->session->flashdata('eac_seo_keywords');} else { echo $eac_seo_keywords; } ?>" />
    		</div>
                        
            <!-- SEO-BESKRIVNING -->
    		<div class="field">
    			<label class="" for="eac_seo_description"><?PHP echo lang('activities_seo_description'); ?></label>
    			<textarea  class="xlarge" rel="tiny" cols="50" rows="7" name="eac_seo_description" id="eac_seo_description"><?php if($this->session->flashdata('eac_seo_description')) { echo $this->session->flashdata('eac_seo_description');} else { echo $eac_seo_description; } ?></textarea>
    		</div>

            <!-- KONTO -->
            <div class="field">
		    	<label><?PHP echo lang('system_choose') . ' ' . lang('system_account'); ?>:</label>
		        <select name="ea_account_id">
					<option value="-1"></option>
		    	<?PHP
					// Skriver ut konton
					// Det konto som posten tillhör sätts som förval i listan
			    	  foreach ($accounts as $post) { ?>
			    		<option value="<?PHP echo $post->account_id; ?>" <?PHP echo ($ea_account_id == $post->account_id) ? 'selected="selected"' : ''; ?>>
						<?PHP echo $post->account_title; ?>
	                    </option>
			    <?php } ?>
		    	</select>
            </div>


            <!-- PUBLICERAD -->
    		<div class="field">
    			<label class="required" for="ea_published"><?PHP echo lang('system_published'); ?></label>
				<select id="ea_published" name="ea_published">
				    <option value="1" <?php echo ($ea_published == 1) ? 'selected="selected"' : ''; ?>><?php echo lang('system_published') ?></option>
				    <option value="0" <?php echo ($ea_published == 0) ? 'selected="selected"' : ''; ?>><?php echo lang('system_draft') ?></option>
				</select>
    		</div>

            <!-- LÄGG TILL -->
       		<div class="buttonrow">
       			<input type="submit" name="save" id="save" value="<?PHP echo lang('system_save'); ?>" class="btn" />
    		</div>
            
    	</form>
    </div>
</div>