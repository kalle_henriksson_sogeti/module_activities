<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');

	
	
	/**
	@Module:		Activities
	@Name:			activities_helper.php
	---------------------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	---------------------------------------------------------------------------------------------------------------
	@Description	Denna fil inneh�ller gemensamma funktioner f�r hanteringen av aktiviteter p� webbplatsen.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade modulen.
		
	*/




	/**
	@Name:			_activities_get_activities_data($language, $published, $status=null, $account_id=null)
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			
	
	@Return						
	---------------------------------------------------------------------------------------------------------------
	@Description: 	Funktionen h�mtar in alla activities baserat p� spr�k, publicerad-flagga och status-flagga.
	
	@History
	DATE			AUTHOR				ACTION
	2015-03-09		Kalle Henriksson	Skapade funktionen.
		
	*/
    function _activities_get_activities_data($language, $published=0, $status=null, $account_id=null)
	{
		$ci = & get_instance();
		
		//	Filtrera p� angivet spr�k
		$ci->db->join('events_activities_content','events_activities_content.eac_ea_id = events_activities.ea_id');
		$ci->db->where('events_activities_content.eac_language', $language);
		
		//	Filtrera p� publicerad
		if($published)
		{			
			$ci->db->where('events_activities.ea_published', $published);
		}
		
		//	Filtrera p� konto-id
		if($account_id != null)
		{			
			$ci->db->where('events_activities.ea_account_id', $account_id);
		}
		
		//	Filtrera p� status
		if($status != null){
			$ci->db->where('events_activities.ea_status', $status);
		}

		$query	=	$ci->db->get('events_activities');

		return $query;

	}

	function _activities_get_activity_data($activity_id, $language){
		$ci = & get_instance();

		$ci->db->limit(1);

		$ci->db->where('events_activities.ea_id', $activity_id);
		$ci->db->join('events_activities_content','events_activities_content.eac_ea_id = events_activities.ea_id');
		$ci->db->where('events_activities_content.eac_language', $language);

		$result = $ci->db->get('events_activities')->row();

		return $result;
	}


	
?>