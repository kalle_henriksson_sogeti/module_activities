<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	
	/**
	@Module:		Activities
	@Name:			activities_lang.php
	@Language:		Svenska
	--------------------------------------------------------------------------------------------------
	@Creator:		Sogeti, Kalle Henriksson
	@Created:		2015
	@Version:		1.0
	@PHP Version: 	5	
	--------------------------------------------------------------------------------------------------
	@Description	Denna fil hanterar språkvariablar för modulen.
	
	@History
	DATUM			VEM					ÅTGÄRD
	2015-03-09		Kalle Henriksson	Skapade funktionen.
	
	*/
	
	$lang['activities_module_name']						=	"activities";
	$lang['activities_headline']						=	"Aktiviteter";
	$lang['activities_add_activity']					=	"Lägg till aktivitet";
	$lang['activities_edit_activity']					=	"Redigera aktivitet";
	$lang['activities_title']							=	"Aktivitetsnamn";
	$lang['activities_activity_was_saved']				=	"Aktiviteten har sparats";
	$lang['activities_seo_keywords']					=	"SEO -nyckelord";
	$lang['activities_seo_description']					=	"SEO -beskrivning";
?>