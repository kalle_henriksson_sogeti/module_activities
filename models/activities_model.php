<?PHP  

	if (!defined('BASEPATH')) exit('No direct script access allowed');
	
	
/**
@Module:		Events
@Name:			activities_model.php
--------------------------------------------------------------------------------------------------
@Creator:		Sogeti, Kalle Henriksson
@Created:		2015
@Version:		1.0
@PHP Version: 	5	
--------------------------------------------------------------------------------------------------
@Description	Denna fil hanterar eventmodulen.
				
@History
DATE			AUTHOR				ACTION
2015-02-23		Kalle Henriksson		Skapade modulen.
	
*/
class Activities_model extends MY_Model 
{
	
	function __construct()
    {
       
	    parent::Model();
		
    }




    
	
	/**
	@Name:			get_all_activities()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param
	
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar alla event för ett visst språk. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson		Skapade funktionen
		
	*/
    function get_all_activities($language='sv')
	{	
		$query = _activities_get_activities_data($language)->result();
		
		return $query;
	}	





    
	
	/**
	@Name:			get_activities()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$language, $published, $status, $account_id
					
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar alla event som inte är borttagna, för ett visst språk. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson		Skapade funktionen
	*/
    function get_activities($language='sv', $account_id = null, $published = null)
	{	
		$query = _activities_get_activities_data($language, $published, 1, $account_id)->result();
		
		return $query;
	}





    
	
	/**
	@Name:			get_activity()
	---------------------------------------------------------------------------------------------------------------
	@Creator: 		Kalle Henriksson
	@Contact: 		www.sogeti.se
	@Created: 		2015
	@Version:		1.0
	
	@Param			$event_id, $language
		
	@return			result_array()					
	---------------------------------------------------------------------------------------------------------------
	@Description:	Hämtar ett event för ett visst språk baserat på event_id. 	
	
	@History
	DATE			AUTHOR				ACTION
	2015-02-23		Kalle Henriksson		Skapade funktionen
		
	*/
	function get_activity($activity_id, $language='sv'){
		$result = _activities_get_activity_data($activity_id, $language);
		
		return $result;
	}	





} 